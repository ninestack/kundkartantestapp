﻿using Kundkartan.Shared.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SignalHub.Shared.Loaders;
using SignalHub.Shared.Services;

namespace Kundkartan.Shared.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddKundkartanServices(this IServiceCollection services, IConfiguration config)
        {
            System.Net.Http.HttpClient httpClient = new System.Net.Http.HttpClient();
            services.AddSingleton<PolygonSourceService>();

            return services;
        }
    }
}
