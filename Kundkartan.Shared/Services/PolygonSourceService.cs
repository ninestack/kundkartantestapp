﻿using SignalHub.Shared.Loaders;
using SignalHub.Shared.Loaders.Abstractions;
using SignalHub.Shared.Models;
using SignalHub.Shared.Services;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Kundkartan.Shared.Services
{
    public class PolygonSourceService
    {
        const short LoaderCount = 1;
        public enum SourceLoaderName 
        { 
            Test
        }
        public ConcurrentDictionary<SourceLoaderName, Loader> Loaders { get; private set; } = new ConcurrentDictionary<SourceLoaderName, Loader>();
        public SourceService SourceService { get; set; }
        public SourceLoader SourceLoader { get; set; }
        
        private string GetSourceId(SourceLoaderName key) => key switch
        {
            SourceLoaderName.Test => "c26a6c3a-d87d-493a-86f9-dfc4cab28733",
            _ => throw new NotImplementedException()
        };

       
        public async Task LoadSourceLoadersAsync()
        {
            // update const short LoaderCount = 14 above if adding more tasks
            var tasks = new Task<Source>[LoaderCount];
            tasks[0] = SourceService.Get(GetSourceId(SourceLoaderName.Test));
            // follow the same ordering as above below..
            var sources = await Task.WhenAll(tasks);
            Loaders.AddOrUpdate(SourceLoaderName.Test, SourceLoader.CreateFrom(sources[0]), (_, __) => SourceLoader.CreateFrom(sources[0]));
        }

        public bool IsReady()
        {
            var notready = default(bool);
            if (Loaders.Keys.Count < LoaderCount) return notready;
            foreach(var kvp in Loaders)
            {
                if (kvp.Value is null) return notready;
            }
            return !notready;
        }
    }
}
