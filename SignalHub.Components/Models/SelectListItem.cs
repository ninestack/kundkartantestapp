﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SignalHub.Components.Models
{
    public class SelectListItem
    {
        public string Value { get; set; }
        public string Title { get; set; }
        public bool Selected { get; set; }
    }
}
