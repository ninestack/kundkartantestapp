﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using SignalHub.Shared.Extensions;
using SignalHub.Shared.Models;
using SignalHub.Shared.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignalHub.Components.Visuals.Base
{
    public abstract class Component : ComponentBase, IDisposable
    {
        [Parameter] public Shared.Loaders.Abstractions.Loader Loader { get; set; }
        [Parameter] public Visual Visual { get; set; }
        [Parameter] public bool Editable { get; set; } = false;


        [Inject]
        protected IJSRuntime JSRuntime { get; set; }
        [Inject]
        protected SourceService SourceService { get; set; }
        [Inject]
        protected Shared.Services.SourceTranslator Translator { get; set; }
        [Inject]
        protected DataNormalizerService DataNormalizerService { get; set; }

        private Source _visualSource { get; set; }
        protected bool SuppressFiltering { get; set; } = false; // Set to true when filtering and do not want the filter event back. Set to false when finihed own filtering

        #region Abstract and overloads methods
        public abstract Task AddMeasure(FieldMeasure field);
        public abstract Task RemoveMeasure(FieldMeasure field);
        public abstract Task AddDimension(FieldDimension field);
        public abstract Task RemoveDimension(FieldDimension field);
        protected virtual async Task RenderJSComponents(bool firstRender) { }
        protected virtual async Task OnBeforeLoad() { }
        protected virtual async Task OnAfterLoad() { }
        #endregion

        #region Rendering and loading
        //protected override void OnInitialized()
        //{
        //    //Loader.OnFilterSet += OnLoaderFilterChanged;
        //    //Loader.OnFilterRemoved += OnLoaderFilterChanged;
        //    //Loader.OnSearchSet += OnLoaderSearchChanged;
        //    //Loader.OnSearchCleared += OnLoaderSearchChanged;
        //    Loader.OnLoad += OnLoaderLoad;
        //}

        public void Dispose()
        {
            Loader.OnLoad -= OnLoaderLoad;
        }

        protected override void OnParametersSet()
        {
            //Loader.OnFilterSet += OnLoaderFilterChanged;
            //Loader.OnFilterRemoved += OnLoaderFilterChanged;
            //Loader.OnSearchSet += OnLoaderSearchChanged;
            //Loader.OnSearchCleared += OnLoaderSearchChanged;
            //Loader.OnLoad += OnLoaderLoad;
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            await RenderJSComponents(firstRender);
            if (firstRender)
            {
                Loader.OnLoad += OnLoaderLoad;
                await Reload();
            }
        }

        public virtual async Task<Source> GetVisualSource()
        {
            if (_visualSource == null)
                _visualSource = await SourceService.Get(Visual.SourceId);

            return _visualSource;
        }

        public virtual async Task Reload() //IEnumerable<FieldFilter> additionalFilters = null, string searchText = null)
        {
            if (CanLoadData())
            {
                var additionalFilters = Loader.Filters.Select(f => f.Filter);
                var searchText = Loader.SearchText;
                await OnBeforeLoad();
                var data = await LoadData(additionalFilters, searchText);
                await RenderData(data);
                await OnAfterLoad();
            }
            else
                await ClearData();
        }

        private bool isLoading = false;
        private async Task OnLoaderLoad(Shared.Loaders.Abstractions.Loader loader, Shared.Loaders.Abstractions.LoadEventArgs eventArgs)
        {
            if (!SuppressFiltering && !isLoading)
            {
                isLoading = true;
                await Reload();
                isLoading = false;
            }
        }

        protected virtual async Task<object> LoadData(IEnumerable<FieldFilter> additionalFilters = null, string searchText = null)
        {
            var payload = Shared.Services.Payloads.AggregationPayload.CreateFrom(Visual.Config, Visual.Appearance?.Rows ?? 50, additionalFilters, searchText);
            var data = await Loader.Aggregate<dynamic>(payload);
            return PrepareData(data);
        }

        protected virtual object PrepareData(object rawData)
        {
            //return DataNormalizerService.NormalizeSingleDimension(rawData as IEnumerable<dynamic>);
            return rawData;
        }

        protected virtual bool CanLoadData() { return Visual.Config.MeasureFields.Any() && Visual.Config.DimensionFields.Any(); }

        protected virtual async Task ClearData() { }

        protected abstract Task RenderData(object data);
        #endregion

        #region Settings
        public virtual async Task NotifySettingsChanged()
        {
            await RenderJSComponents(firstRender: false);
        }
        #endregion

        protected IDictionary<string, object> GetDictionaryFromJsonElement(System.Text.Json.JsonElement obj)
        {
            if (obj.ValueKind != System.Text.Json.JsonValueKind.Object)
                throw new Exception("Json element is not an object");

            var properties = new Dictionary<string, object>();
            foreach(var property in obj.EnumerateObject())
            {
                var name = property.Name;
                var value = GetValueFromJsonElement(property.Value);
                properties.Add(name, value);
            }
            return properties;
        }

        protected object GetValueFromJsonElement(System.Text.Json.JsonElement value)
        {
            switch (value.ValueKind)
            {
                case System.Text.Json.JsonValueKind.False:
                    return false;
                case System.Text.Json.JsonValueKind.True:
                    return true;
                case System.Text.Json.JsonValueKind.Number:
                    if (value.TryGetInt64(out var _int))
                        return _int;
                    return value.GetDouble();
                case System.Text.Json.JsonValueKind.String:
                    return value.GetString();
                default:
                    return value.GetRawText();
            }
        }

        protected virtual void SetDimensionAndMeasureResultNames()
        {
            Visual.Config.DimensionFields.ToList().ForEach(field => SetNextResultName(field));
            Visual.Config.MeasureFields.ToList().ForEach(field => SetNextResultName(field));
            //Update(Visual.Config.OrderFields);
            var allQueryFields = new List<DrillField>().AddMany(Visual.Config.DimensionFields, Visual.Config.MeasureFields);
            foreach (var orderField in Visual.Config.OrderFields)
            {
                var field = allQueryFields.FirstOrDefault(f => f.Name == orderField.Name);
                if (field != null)
                    orderField.ResultName = string.IsNullOrWhiteSpace(field.ResultName) ? field.Name : field.ResultName;
            }
        }

        protected string TranslateResultNameToFieldName(string resultName)
        {
            if (string.IsNullOrWhiteSpace(resultName))
                return null;

            var dimensionField = Visual.Config.DimensionFields.FirstOrDefault(f => resultName.Equals(f.ResultName));
            if (dimensionField != null)
                return dimensionField.Name;
            var measureField = Visual.Config.MeasureFields.FirstOrDefault(f => resultName.Equals(f.ResultName));
            if (measureField != null)
                return measureField.Name;

            return null;
        }

        protected virtual void SetNextResultName(FieldDimension field)
        {
            if (!string.IsNullOrWhiteSpace(field.ResultName))
                return;
            field.ResultName = GetNextResultName(Visual.Config.DimensionFields, "key");

            //var i = 0;
            //while (true)
            //{
            //    var measureField = Visual.Config.DimensionFields.FirstOrDefault(m => m.ResultName == $"key{i}");
            //    if (measureField == null)
            //    {
            //        field.ResultName = $"key{i}";
            //        break;
            //    }
            //    i++;
            //}
        }

        protected virtual void SetNextResultName(FieldMeasure field)
        {
            if (!string.IsNullOrWhiteSpace(field.ResultName))
                return;

            field.ResultName = GetNextResultName(Visual.Config.MeasureFields, "value");
            //var i = 0;
            //while (true)
            //{
            //    var dimensionField = Visual.Config.MeasureFields.FirstOrDefault(m => m.ResultName == $"value{i}");
            //    if (dimensionField == null)
            //    {
            //        field.ResultName = $"value{i}";
            //        break;
            //    }
            //    i++;
            //}
        }

        protected virtual string GetNextResultName(IEnumerable<DrillField> fields, string fieldPrefix)
        {
            var i = 0;
            while (true)
            {
                var dimensionField = fields.FirstOrDefault(m => m.ResultName == $"{fieldPrefix}{i}");
                if (dimensionField == null)
                {
                    return $"{fieldPrefix}{i}";
                }
                i++;
            }

        }
    }
}
