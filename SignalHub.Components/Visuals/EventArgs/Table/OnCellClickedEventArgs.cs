﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SignalHub.Components.Visuals.EventArgs.Table
{
    public class OnCellClickedEventArgs
    {
        public string FieldResultName { get; set; }
        public int RowIndex { get; set; }
        public object Value { get; set; }
    }
}
