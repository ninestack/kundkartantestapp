﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SignalHub.Components.Visuals.EventArgs.Table
{
    public class OnColumnMovedEventArgs
    {
        public string FieldResultName { get; set; }
        public int MovedToIndex { get; set; }
    }
}
