﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SignalHub.Components.Visuals.EventArgs.Table
{
    public class OnColumnResizedEventArgs
    {
        public string FieldResultName { get; set; }
        public int Width { get; set; }
    }
}
