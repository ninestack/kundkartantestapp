﻿using SignalHub.Shared.Extensions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SignalHub.Shared.Models;

namespace SignalHub.Components.Visuals
{
    public partial class Metric
    {
        private string MetricFormatted { get; set; }
        private MetricAppearance Settings { get; set; }

        public override async Task NotifySettingsChanged()
        {
            StateHasChanged();
        }

        protected override async Task RenderData(object data)
        {
            // TODO: Get locale from user/browser
            //var locale = this.req Request.HttpContext.Features.Get<IRequestCultureFeature>();
            //var BrowserCulture = locale.RequestCulture.UICulture.ToString();
            var norwegianCulture = System.Globalization.CultureInfo.GetCultureInfo("nb-NO");
            var metric = data as double?;
            MetricFormatted = metric.HasValue ? metric.Value.ToString("N0", norwegianCulture) : string.Empty;
            StateHasChanged();
        }

        public override async Task RemoveMeasure(Shared.Models.FieldMeasure field)
        {
            await base.RemoveMeasure(field);
            MetricFormatted = string.Empty;
        }


        protected override void OnInitialized()
        {
            base.OnInitialized();
            Settings = Visual.Appearance as SignalHub.Shared.Models.MetricAppearance ?? new Shared.Models.MetricAppearance();
        }

        protected override void OnAfterRender(bool firstRender)
        {
        }

        protected virtual string GetTitleStyle()
        {
            return "font-size: 16px;";
        }

        protected virtual string GetMetricStyle()
        {
            return "font-size:55px;font-weight:bold;";
        }

        protected virtual string GetSubTitleStyle()
        {
            return "font-size:13px;";
        }
    }
}
