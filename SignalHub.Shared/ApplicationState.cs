﻿using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SignalHub.Shared
{
    public class ApplicationState
    {
        private readonly Services.UserService _userService;
        private readonly Services.CompanyService _companyService;
        private readonly AuthenticationStateProvider _authenticationStateProvider;
        private readonly IConfiguration _configuration;
        private readonly object UserQueryLock = new object();

        public ApplicationState(Services.UserService userService, Services.CompanyService companyService, AuthenticationStateProvider authenticationStateProvider, IConfiguration configuration)
        {
            _userService = userService;
            _companyService = companyService;
            _authenticationStateProvider = authenticationStateProvider;
            _configuration = configuration;
        }

        //public string AppId { get; set; } = "4beb01d2-caf1-4993-9c12-5f7cb9da8c0d";
        //public string ApiSecret { get; set; } = "1f5ceb5f-5adf-481b-90bf-5778ccfb4437";

        //public Models.User User { get; set; } = new Models.User { Id = "83050be8-0b12-4114-af30-dc31979e3ff7", Name = "Stein-Erik Remme", Language = "no" };
        //public Models.Company Company { get; set; } = new Models.Company { Id = "", Name = "Prognosesenteret", Url = "prognosesenteret", AppId = "4beb01d2-caf1-4993-9c12-5f7cb9da8c0d", ApiSecret = "1f5ceb5f-5adf-481b-90bf-5778ccfb4437" };

        //public Models.User User
        //{
        //    get
        //    {
        //        return new Models.User { Id = string.Empty, Name = string.Empty, Language = "en" };
        //    }
        //}
        //public Models.Company Company { get; set; } = new Models.Company { Id = string.Empty, Name = string.Empty, Url = string.Empty, AppId = string.Empty, ApiSecret = string.Empty };

        private Models.User _user = null;
        public async Task<Models.User> User()
        {
            await EnsureUserOrDefault();
            return _user;
        }
        //public async Task<Models.User> User()
        //{
        //    var state = await _authenticationStateProvider.GetAuthenticationStateAsync();
        //    return await User(state.User);
        //}

        private Models.Company _company = null;
        public async Task<Models.Company> Company()
        {
            await EnsureUserOrDefault();
            if (_user != null)
            {
                if (_company == null)
                    _company = await _companyService.GetCompany(_user.CompanyId);
                if (_company != null)
                    return _company;
            }
            return new Models.Company { Id = string.Empty, Name = string.Empty, Url = string.Empty, AppId = string.Empty, ApiSecret = string.Empty };
        }
        //public async Task<Models.Company> Company()
        //{
        //    var state = await _authenticationStateProvider.GetAuthenticationStateAsync();
        //    return await Company(state.User);
        //}
        //public void Company(Models.Company company) => _company = company;


        public async Task<IEnumerable<Models.Source>> LatestSources()
        {
            await EnsureUserOrDefault("User is not authenticated and has not privilege to fetch latest sources.");
            return await _userService.GetLatestSources(_user.Id);
        }

        public async Task AppendLatestSource(string sourceId)
        {
            await EnsureUserOrDefault("User is not authenticated and has not privilege to append to latest sources.");
            await _userService.AppendLatestSource(sourceId, _user.Id);
        }

        public async Task<IEnumerable<Models.Source>> StarredSources()
        {
            await EnsureUserOrDefault("User is not authenticated and has not privilege to fetch starred sources.");
            return await _userService.GetStarredSources(_user.Id);
        }

        public async Task AppendStarredSource(string sourceId)
        {
            await EnsureUserOrDefault("User is not authenticated and has not privilege to append to starred sources.");
            await _userService.AppendStarredSources(sourceId, _user.Id);
        }

        public async Task<IEnumerable<Models.Visual>> LatestVisuals(int count = 4)
        {
            await EnsureUserOrDefault("User is not authenticated and has not privilege to fetch latest visuals.");
            return await _userService.GetLatestVisuals(_user.Id, count);
        }

        public async Task AppendLatestVisual(string visualId)
        {
            await EnsureUserOrDefault("User is not authenticated and has not privilege to append to latest visuals.");
            await _userService.AppendLatestVisual(visualId, _user.Id);
        }

        private async Task EnsureUserOrDefault(string exceptionMessage = null)
        {
            if (_user == null)
            {
                // We double check that user is null before locking and loading
                var state = await _authenticationStateProvider.GetAuthenticationStateAsync();
                if (_user == null && state.User.Identity.IsAuthenticated)
                {
                    lock (UserQueryLock)
                    {
                        if (_user == null)
                        {
                            var userId = GetDefaultUser() ?? state.User.Identity.Name;
                            _user = Task.Run(() => _userService.GetByEmail(userId)).Result;
                        }
                    }
                }
            }
            if (_user == null)
            {
                if (string.IsNullOrEmpty(exceptionMessage))
                    _user = new Models.User { Id = string.Empty, Name = string.Empty, Language = "no" };
                else
                    throw new Exception(exceptionMessage);
            }
        }

        private string GetDefaultUser()
        {
            return _configuration.GetSection("ApplicationSettings").GetValue<string>("DefaultUser");
        }
    }
}
