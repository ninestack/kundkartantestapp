﻿using SignalHub.Shared.Models;
using System.Collections.Generic;
using System.Linq;

namespace SignalHub.Shared.Extensions
{
    public static class FieldDimensionList
    {
        public static IEnumerable<DrillField> OrderWith(this IEnumerable<DrillField> list, IEnumerable<TableFieldAppearance> orderByFields)
        {
            var orderedList = new List<DrillField>();
            foreach (var orderByField in orderByFields.Where(f => f != null).OrderBy(f => f.Index))
            {
                var item = list.FirstOrDefault(f => f.ResultName == orderByField.Name);
                orderedList.AddIfNotNull(item);
            }
            var rest = list.Except(orderedList);
            orderedList.AddRange(rest);
            return orderedList;
        }
    }
}
