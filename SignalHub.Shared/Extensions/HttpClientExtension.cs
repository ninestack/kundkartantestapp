﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace SignalHub.Shared.Extensions
{
    public static class HttpClientExtension
    {
        public static async Task<T> GetJsonSignalHubAsync<T>(this HttpClient client, string uri, object content = null)
        {
            return await client.ReceiveJsonSignalHubAsync<T>("GET", uri, content);
        }

        public static async Task<T> PostJsonSignalHubAsync<T>(this HttpClient client, string uri, object content)
        {
            return await client.ReceiveJsonSignalHubAsync<T>("POST", uri, content);
        }

        public static async Task<T> PutJsonSignalHubAsync<T>(this HttpClient client, string uri, object content)
        {
            return await client.ReceiveJsonSignalHubAsync<T>("PUT", uri, content);
        }

        public static async Task<T> PatchJsonSignalHubAsync<T>(this HttpClient client, string uri, object content)
        {
            return await client.ReceiveJsonSignalHubAsync<T>("PATCH", uri, content);
        }

        public static async Task<T> DeleteJsonSignalHubAsync<T>(this HttpClient client, string uri)
        {
            return await client.ReceiveJsonSignalHubAsync<T>("DELETE", uri, content: null);
        }

        public static async Task<T> DeleteJsonSignalHubAsync<T>(this HttpClient client, string uri, object content)
        {
            return await client.ReceiveJsonSignalHubAsync<T>("DELETE", uri, content);
        }

        private static async Task<T> ReceiveJsonSignalHubAsync<T>(this HttpClient client, string method, string uri, object content)
        {
            HttpRequestMessage requestMessage = null;

            var options = new System.Text.Json.JsonSerializerOptions
            {
                PropertyNamingPolicy = System.Text.Json.JsonNamingPolicy.CamelCase,
                Encoder = System.Text.Encodings.Web.JavaScriptEncoder.Create()
            };
            options.Converters.Add(new JsonConverters.DynamicNewtonsoftCompatibleConverter());
            //options.Converters.Add(new JsonConverters.SystemObjectNewtonsoftCompatibleConverter());
            //options.Converters.Add(new JsonConverters.VisualSettingsObjectConverter());



            if (content == null)
                requestMessage = new HttpRequestMessage { Method = new HttpMethod(method), RequestUri = new Uri($"{client.BaseAddress}{uri}"), Content = new StringContent(string.Empty) };
            else
            {
                //var options = new System.Text.Json.JsonSerializerOptions { };
                //options.Converters.Add()
                var json = System.Text.Json.JsonSerializer.Serialize(content, options);
                requestMessage = new HttpRequestMessage { Method = new HttpMethod(method), RequestUri = new Uri($"{client.BaseAddress}{uri}"), Content = new StringContent(json) };
                requestMessage.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            }
            requestMessage.Headers.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

            var response = await client.SendAsync(requestMessage);

            if (response.IsSuccessStatusCode)
            {
                var responseBody = await response.Content.ReadAsStringAsync();
                if (string.IsNullOrWhiteSpace(responseBody))
                    return default(T);

                var result = System.Text.Json.JsonSerializer.Deserialize<T>(responseBody, options);
                return result;
            }
            throw new Exception($"No success. Status code {response.StatusCode}. {response.ReasonPhrase}");
        }
    }
}
