﻿using System;
using System.Collections.Generic;

namespace SignalHub.Shared.Extensions
{
    public static class ListExtension
    {
        public static List<T> AddMany<T>(this List<T> me, params IEnumerable<T>[] items)
        {
            foreach (var item in items)
                me.AddRange(item);
            return me;
        }

        public static List<T> AddIfNotNull<T>(this List<T> me, T obj)
        {
            if (obj != null)
                me.Add(obj);
            return me;
        }

        public static void AddIf<T>(this List<T> me, bool shouldAdd, Func<T> valueFn)
        {
            if (shouldAdd)
                me.Add(valueFn());
        }

        public static void AddIf<T>(this List<T> me, bool shouldAdd, Func<T> valueFn, Func<T> valueFnElse)
        {
            if (shouldAdd)
                me.Add(valueFn());
            else
                me.Add(valueFnElse());
        }
    }
}
