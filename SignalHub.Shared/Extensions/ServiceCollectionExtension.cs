﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
//using Microsoft.AspNetCore.Authentication;
//using Microsoft.AspNetCore.Authentication.OpenIdConnect;
//using Microsoft.AspNetCore.Authentication.Cookies;
//using Microsoft.IdentityModel.Tokens;
//using Microsoft.Extensions.DependencyInjection;

namespace SignalHub.Shared.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddSignalHub(this IServiceCollection services, string applicationName, IConfigurationSection applicationSettingsSection)
        {
            if (applicationSettingsSection.GetSection("AzureSignalREndpoint").Value == null)
            {
                //services.AddSignalR(options =>
                //{
                //    options.EnableDetailedErrors = true;
                //    //options.KeepAliveInterval = TimeSpan.FromMinutes(1);
                //});
                return services;
            }
            var azureSignalRServiceEndpoint = applicationSettingsSection.GetSection("AzureSignalREndpoint").Get<string>();
            services.AddSignalR().AddAzureSignalR(options =>
            {
                options.ConnectionString = azureSignalRServiceEndpoint;
                options.ApplicationName = applicationName;
                options.ServerStickyMode = Microsoft.Azure.SignalR.ServerStickyMode.Required;
            });
            return services;
        }

        public static IServiceCollection AddSignalHubServices(this IServiceCollection services, IConfigurationSection applicationSettingsSection)
        {
            services.AddHttpContextAccessor();
            services.AddScoped<Microsoft.AspNetCore.Http.HttpContextAccessor>();

            // Loaders
            services.AddScoped<Loaders.LoaderManager>();
            services.AddScoped<Loaders.SourceLoader>();

            // Application state and services
            services.AddScoped<ApplicationState>();
            services.AddSingleton<Services.DataNormalizerService>();
            services.AddScoped<Services.SourceTranslator>();
            services.AddSingleton<Services.QueryEngineService>();
            services.AddScoped<Services.FilterTranslator>();

            services.AddHttpClient<Services.SourceService>("SignalHubSourceService", (provider, httpClient) =>
            {
                SetSignalHubUri(httpClient, applicationSettingsSection);
                SetSignalHubHeader(httpClient, applicationSettingsSection);
            });
            services.AddHttpClient<Services.VisualService>("SignalHubVisualService", (provider, httpClient) =>
            {
                SetSignalHubUri(httpClient, applicationSettingsSection);
                SetSignalHubHeader(httpClient, applicationSettingsSection);
            });
            services.AddHttpClient<Services.UserService>("SignalHubUserService", (provider, httpClient) =>
            {
                SetSignalHubUri(httpClient, applicationSettingsSection);
                SetSignalHubHeader(httpClient, applicationSettingsSection);
            });
            services.AddHttpClient<Services.CompanyService>("SignalHubCompanyService", (provider, httpClient) =>
            {
                SetSignalHubUri(httpClient, applicationSettingsSection);
                SetSignalHubHeader(httpClient, applicationSettingsSection);
            });
            services.AddHttpClient<Services.DashboardService>("SignalHubDashboardService", (provider, httpClient) =>
            {
                SetSignalHubUri(httpClient, applicationSettingsSection);
                SetSignalHubHeader(httpClient, applicationSettingsSection);
            });
            services.AddHttpClient<Services.ApplicationService>("SignalHubApplicationService", (provider, httpClient) =>
            {
                SetSignalHubUri(httpClient, applicationSettingsSection);
                SetSignalHubHeader(httpClient, applicationSettingsSection);
            });

            services.AddHttpClient<Services.SignalQueryService>("SignalHubQueryService", (provider, httpClient) =>
            {
                // TODO: API should be merged into the new API
                SetSignalHubUri(httpClient, applicationSettingsSection);
                SetSignalHubHeader(httpClient, applicationSettingsSection);
            });

            return services;
        }

        private static void SetSignalHubUri(System.Net.Http.HttpClient httpClient, IConfigurationSection applicationSettingsSection)
        {
            var signalHubUri_v2 = applicationSettingsSection.GetSection("SignalHubAPIUri").Get<string>();
            httpClient.BaseAddress = new Uri(signalHubUri_v2);
        }

        private static void SetSignalHubHeader(System.Net.Http.HttpClient httpClient, IConfigurationSection applicationSettingsSection)
        {
            var appId = applicationSettingsSection.GetSection("SignalHubAppId").Get<string>();
            var apiSecret = applicationSettingsSection.GetSection("SignalHubApiSecret").Get<string>();
            httpClient.DefaultRequestHeaders.Add("X-App-Id", appId);
            httpClient.DefaultRequestHeaders.Add("X-Api-Secret", apiSecret);
        }
    }
}
