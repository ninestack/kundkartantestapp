﻿using Newtonsoft.Json;
using System;

namespace SignalHub.Shared.JsonConverters
{
    public class NewtonsoftConvertStandard : JsonConverter
    {
        public override bool CanConvert(Type objectType) => true;
        public override bool CanRead => true;
        public override bool CanWrite => true;

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var defaultTypeNameHandling = serializer.TypeNameHandling;
            serializer.TypeNameHandling = TypeNameHandling.All;
            object obj = serializer.Deserialize(reader);
            serializer.TypeNameHandling = defaultTypeNameHandling;
            return obj;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteStartObject();
            WriteTypeProperty(writer, value);
            //foreach (var property in value.GetType().GetProperties())
            //{
            //    var propertyName = options.PropertyNamingPolicy == null ? property.Name : options.PropertyNamingPolicy.ConvertName(property.Name);
            //    var propertyValue = property.GetValue(value);
            //    writer.WritePropertyName(propertyName);
            //    //JsonSerializer.Serialize(writer, propertyValue, propertyValue.GetType(), options);
            //    serializer.Serialize(writer, value);
            //}
            serializer.Serialize(writer, value);
            writer.WriteEndObject();
        }

        private void WriteTypeProperty(JsonWriter writer, object value)
        {
            var valueType = value.GetType();
            var typeName = $"{valueType.FullName}, {valueType.Assembly.GetName().Name}";
            writer.WritePropertyName("$type");
            writer.WriteValue(typeName);
        }
    }
}
