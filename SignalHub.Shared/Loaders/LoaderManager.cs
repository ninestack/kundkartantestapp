﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalHub.Shared.Loaders
{
    // This should be deleted and exchanged with new Dimension implementation
    public class LoaderManager
    {
        private System.Collections.Concurrent.ConcurrentDictionary<string, Abstractions.Loader> Loaders { get; set; } = new System.Collections.Concurrent.ConcurrentDictionary<string, Abstractions.Loader>();
        //private bool _isSettingFilter = false;
        private System.Collections.Concurrent.ConcurrentDictionary<string, Abstractions.Loader> LoadingLoaders = new System.Collections.Concurrent.ConcurrentDictionary<string, Abstractions.Loader>();

        public event Func<Abstractions.Loader, Abstractions.FilterChangeEventArgs, Task> OnFilterSet;
        public event Func<Abstractions.Loader, Abstractions.FilterChangeEventArgs, Task> OnFilterRemoved;
        public event Func<Abstractions.Loader, string, Task> OnSearchSet;
#pragma warning disable CS0067 // The event 'LoaderManager.OnSearchCleared' is never used
        public event Func<Abstractions.Loader, string, Task> OnSearchCleared;
#pragma warning restore CS0067 // The event 'LoaderManager.OnSearchCleared' is never used
        public event Func<Abstractions.Loader, Abstractions.LoadEventArgs, Task> OnLoad;

        public Abstractions.Loader GetLoader(string name)
        {
            if (Loaders.ContainsKey(name))
                return Loaders[name];

            return null;
        }

        public IEnumerable<Abstractions.Loader> GetAll() => Loaders.Values;

        public bool TryGetLoader(string name, out Abstractions.Loader loader)
        {
            if (Loaders.ContainsKey(name))
                loader = Loaders[name];
            else
                loader = null;

            return loader != null;
        }

        public Abstractions.Loader TryRegister(Abstractions.Loader loader)
        {
            if (Loaders.ContainsKey(loader.Name))
                loader = Loaders[loader.Name];
            else
            {
                loader.OnFilterSet += OnLoaderFilterSet;
                loader.OnFilterRemoved += OnLoaderFilterRemoved;
                loader.OnSearchSet += OnLoaderSearchSet;
                loader.OnSearchCleared += OnLoaderSearchCleared;
                loader.OnLoad += FireLoadEvent;
                Loaders.TryAdd(loader.Name, loader);
            }
            return loader;
        }

        private async Task OnLoaderFilterSet(Abstractions.Loader loader, Abstractions.FilterChangeEventArgs ev)
        {
            await OnFilter(loader, ev, async l => await l.SetFilter(ev.CurrentFilter));
            await HandleEvent(loader, OnFilterSet, ev);
            await FireLoadEvent(loader);
        }

        private async Task OnLoaderFilterRemoved(Abstractions.Loader loader, Abstractions.FilterChangeEventArgs ev)
        {
            await OnFilter(loader, ev, async l => await l.RemoveFilter(ev.CurrentFilter));
            await HandleEvent(loader, OnFilterRemoved, ev);
            await FireLoadEvent(loader);
        }

        private async Task OnLoaderSearchSet(Abstractions.Loader loader, string searchText)
        {
            await HandleEvent(loader, OnSearchSet, searchText);
            await FireLoadEvent(loader);
        }

        private async Task OnLoaderSearchCleared(Abstractions.Loader loader, string searchText)
        {
            await HandleEvent(loader, OnSearchSet, searchText);
            await FireLoadEvent(loader);
        }

        private async Task OnFilter(Abstractions.Loader loader, Abstractions.FilterChangeEventArgs ev, Func<Abstractions.Loader, Task> callback = null)
        {
            LoadingLoaders.TryAdd(loader.Name, loader);
            //if (_isSettingFilter)
            //    return;

            //_isSettingFilter = true;
            foreach (var registeredLoader in Loaders.Values.Where(l => l.Name != loader.Name))
            {
                if (!LoadingLoaders.ContainsKey(loader.Name) && registeredLoader.Fields.Any(f => !(string.IsNullOrWhiteSpace(f.Kind) && string.IsNullOrWhiteSpace(ev.CurrentFilter.Field.Kind)) && f.Kind == ev.CurrentFilter.Field.Kind))
                {
                    if (callback != null)
                        await callback(registeredLoader);
                }
            }
            LoadingLoaders.TryRemove(loader.Name, out _);
            //_isSettingFilter = false;
        }

        private async Task FireLoadEvent(Abstractions.Loader loader, Abstractions.LoadEventArgs loadEventArgs = null)
        {
            if (loadEventArgs == null)
                loadEventArgs = new Abstractions.LoadEventArgs { Filters = loader.Filters, SearchText = loader.SearchText };
            await HandleEvent(loader, OnLoad, loadEventArgs);
        }

        private async Task HandleEvent<E>(Abstractions.Loader loader, Func<Abstractions.Loader, E, Task> handler, E args)
        {
            if (handler == null)
            {
                return;
            }
            Delegate[] invocationList = handler.GetInvocationList();
            Task[] handlerTasks = new Task[invocationList.Length];

            for (int i = 0; i < invocationList.Length; i++)
            {
                handlerTasks[i] = ((Func<Abstractions.Loader, E, Task>)invocationList[i])(loader, args);
            }
            await Task.WhenAll(handlerTasks);
        }
    }
}
