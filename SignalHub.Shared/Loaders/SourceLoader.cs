﻿using SignalHub.Shared.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SignalHub.Shared.Loaders
{
    public class SourceLoader : Abstractions.Loader
    {
        private Services.SignalQueryService QueryService;
        private LoaderManager LoaderManager;
        private Models.Source Source;

        public override string Name { get { return Source.Name; } }
        public override IEnumerable<Models.Field> Fields { get { return Source.Fields; } }

        private SourceLoader() { }
        public SourceLoader(Services.SignalQueryService queryService, LoaderManager loaderManager)
        {
            QueryService = queryService;
            LoaderManager = loaderManager;
        }

        public IDictionary<Source, Abstractions.Loader> CreateFrom(IEnumerable<Source> sources, bool registerInManager = true)
        {
            var loaders = new Dictionary<Source, Abstractions.Loader>();
            foreach (var source in sources)
                loaders.Add(source, CreateFrom(source, registerInManager));
            return loaders;
        }

        public Abstractions.Loader CreateFrom(Source source, bool registerInManager = true)
        {
            if (registerInManager)
            {
                if (LoaderManager.TryGetLoader(source.Name, out var l))
                    return l;
            }
            var loader = new SourceLoader
            {
                QueryService = QueryService,
                Source = source
            };
            if (registerInManager)
                LoaderManager.TryRegister(loader);

            return loader;
        }

        public override async Task<double?> Metric(object payload)
        {
            return await QueryService.Metric(Source, payload);
        }

        public override async Task<IEnumerable<T>> Aggregate<T>(object payload)
        {
            return await QueryService.Aggregation<T>(Source, payload);
        }

        public override async Task<IEnumerable<T>> Tabular<T>(object payload)
        {
            return await QueryService.Tabular<T>(Source, payload);
        }

        public override async Task<IEnumerable<T>> Distinct<T>(object payload)
        {
            return await QueryService.Distinct<T>(Source, payload);
        }
    }
}
