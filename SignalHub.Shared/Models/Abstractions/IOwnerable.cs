﻿namespace SignalHub.Shared.Models.Abstractions
{
    public interface IOwnerable
    {
        string Owner { get; set; }
    }
}
