﻿using System;
using System.Collections.Generic;

namespace SignalHub.Shared.Models
{
    public class Application : Abstractions.IHubEntity, Abstractions.IReadAccessCompanies, Abstractions.IOwnerable
    {
        public string __type { get; } = "application";
        public string Id { get; set; }
        public string Url { get; set; }
        public string StagingUrl { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool ShowFilters { get; set; }
        public bool ShowSearch { get; set; }
        public string LogoUrl { get; set; }
        public IList<Visual> Visuals { get; set; } = new List<Visual>();
        public IList<Dashboard> Dashboards { get; set; } = new List<Dashboard>();

        public string Owner { get; set; }
        public IEnumerable<string> GrantedAccessCompanies { get; set; } = new List<string>();

        public string CreatedBy { get; set; }
        public DateTime? CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool Archived { get; set; }
        public string ArchivedBy { get; set; }
        public DateTime? ArchivedAt { get; set; }
    }
}
