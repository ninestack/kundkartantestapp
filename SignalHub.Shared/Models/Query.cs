﻿using System.Collections.Generic;

namespace SignalHub.Shared.Models
{
    public class Query
    {
        public string SearchText { get; set; }
        public IEnumerable<FieldMeasure> Aggregations { get; set; }
        public IEnumerable<FieldDimension> Dimensions { get; set; }
        public IEnumerable<FieldFilter> Filters { get; set; }
        public IEnumerable<FieldOrder> Orders { get; set; }
        public IEnumerable<Variable> Variables { get; set; }
        public int? Size { get; set; }
    }

    public class DistinctQuery
    {
        public IEnumerable<FieldDimension> Dimensions { get; set; }
        public IEnumerable<FieldFilter> Filters { get; set; }
        public IEnumerable<FieldOrder> Orders { get; set; }
        public IEnumerable<Variable> Variables { get; set; }
        public int? Size { get; set; }
    }

    public class TabularQuery
    {
        public string SearchText { get; set; }
        public IEnumerable<string> Project { get; set; }
        public IEnumerable<FieldMeasure> Aggregations { get; set; }
        public IEnumerable<FieldFilter> Filters { get; set; }
        public IEnumerable<FieldOrder> Orders { get; set; }
        public IEnumerable<Variable> Variables { get; set; }
        public int? Size { get; set; }
    }

    public class CountQuery
    {
        public string SearchText { get; set; }
        public string Field { get; set; }
        public IEnumerable<FieldDimension> BaseDimensions { get; set; }
        public IEnumerable<FieldMeasure> BaseAggregations { get; set; }
        public IEnumerable<FieldMeasure> Aggregations { get; set; }
        public IEnumerable<FieldFilter> Filters { get; set; }
        public IEnumerable<Variable> Variables { get; set; }
        public string Unit { get; set; }
        public long? Duration { get; set; }
    }
}
