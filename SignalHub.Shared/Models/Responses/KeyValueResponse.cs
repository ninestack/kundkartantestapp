﻿namespace SignalHub.Shared.Models.Responses
{
    public class KeyValue
    {
        public object key { get; set; }
        public object value0 { get; set; }
        public object value1 { get; set; }
        public object value2 { get; set; }
        public object value3 { get; set; }
        public object value4 { get; set; }
    }
}
