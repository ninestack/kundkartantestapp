﻿using System.Collections.Generic;

namespace SignalHub.Shared.Models.Responses
{
    public class Metric
    {
        public double? value { get; set; }
    }

    public class Tabular<T>
    {
        public IEnumerable<T> hits { get; set; }
    }

    public class Aggregate<T>
    {
        //public IEnumerable<T> hits { get; set; }
        //public IDictionary<string, IEnumerable<Models.Responses.KeyValue>> aggregations { get; set; }
        public IEnumerable<T> aggregations { get; set; }
    }
}
