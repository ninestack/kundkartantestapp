﻿using System.Threading.Tasks;

namespace SignalHub.Shared.Services.Abstractions
{
    public interface ItemsCollectorExecuter
    {
        bool ExecuteOnStartup { get; }
        int TimeSpanSeconds { get; }
        Task Execute();
    }
}
