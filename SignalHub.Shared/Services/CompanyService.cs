﻿using SignalHub.Shared.Extensions;
using System.Net.Http;
using System.Threading.Tasks;

namespace SignalHub.Shared.Services
{
    public class CompanyService
    {
        private HttpClient httpClient;

        public CompanyService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task<Models.Company> GetCompany(string id)
        {
            var uri = $"company/{id}";
            return await httpClient.GetJsonSignalHubAsync<Models.Company>(uri);
        }
    }
}
