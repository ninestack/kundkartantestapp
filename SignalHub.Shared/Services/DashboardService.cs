﻿using SignalHub.Shared.Extensions;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace SignalHub.Shared.Services
{
    public class DashboardService
    {
        private HttpClient httpClient;

        public DashboardService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task<Models.Dashboard> Get(string dashboardId)
        {
            var uri = $"dashboard/{dashboardId}";
            return await httpClient.GetJsonSignalHubAsync<Models.Dashboard>(uri);
        }

        public async Task<IEnumerable<Models.Dashboard>> All()
        {
            var uri = "dashboard";
            return await httpClient.GetJsonSignalHubAsync<IEnumerable<Models.Dashboard>>(uri);
        }

        public async Task<object> LinkVisual(string dashboardId, string visualId)
        {
            return await LinkVisual(dashboardId, new List<string> { visualId });
        }
        public async Task<object> LinkVisual(string dashboardId, IEnumerable<string> visualIds)
        {
            var uri = $"dashboard/{dashboardId}/visual";
            return await httpClient.PutJsonSignalHubAsync<object>(uri, new { visualIds });
        }

        public async Task<object> UnlinkVisual(string dashboardId, string visualId)
        {
            return await UnlinkVisual(dashboardId, new List<string> { visualId });
        }

        public async Task<object> UnlinkVisual(string dashboardId, IEnumerable<string> visualIds)
        {
            var uri = $"dashboard/{dashboardId}/visual";
            return await httpClient.DeleteJsonSignalHubAsync<object>(uri, new { visualIds });
        }

        public async Task<Models.Dashboard> Upsert(Models.Dashboard dashboard)
        {
            var uri = $"dashboard";
            var updatedDashboard = await httpClient.PostJsonSignalHubAsync<Models.Dashboard>(uri, dashboard);
            //var updateDashboardInApplicationsUri = "dashboard/applications";
            //await httpClient.PostJsonSignalHubAsync<Models.Dashboard>(updateDashboardInApplicationsUri, dashboard);
            return updatedDashboard;
        }
    }
}
