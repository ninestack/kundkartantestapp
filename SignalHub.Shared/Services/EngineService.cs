﻿using System.Collections.Generic;

namespace SignalHub.Shared.Services
{
    public class QueryEngineService
    {
        public IEnumerable<Models.QueryEngine> GetAllEngines()
        {
            return new List<Models.QueryEngine>
            {
                new Models.QueryEngine { Title = "Text", EngineType = Models.QueryEngine.EngineTypeDefinition.Elastic},
                new Models.QueryEngine { Title = "Analytic", EngineType = Models.QueryEngine.EngineTypeDefinition.Kusto}
            };
        }
    }
}
