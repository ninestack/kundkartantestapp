﻿using SignalHub.Shared.Extensions;
using SignalHub.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SignalHub.Shared.Services
{
    public class SignalQueryService
    {
        private HttpClient httpClient;

        public SignalQueryService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task<double?> Metric(Models.Source source, object payload)
        {
            var uri = $"{source.Url}/metric";
            var response = await httpClient.PostJsonSignalHubAsync<Models.Responses.Metric>(uri, payload);
            return response.value;
        }

        //public async Task<IDictionary<string, IEnumerable<Models.Responses.KeyValue>>> Aggregation(Models.Source source, object payload)
        public async Task<IEnumerable<T>> Aggregation<T>(Models.Source source, object payload)
        {
            var uri = $"{source.Url}/aggregation";
            var response = await httpClient.PostJsonSignalHubAsync<IEnumerable<T>>(uri, payload);
            //return response.aggregations;
            return response;
        }

        public async Task<IEnumerable<T>> Tabular<T>(Models.Source source, object payload)
        {
            var uri = $"{source.Url}/tabular";
            var response = await httpClient.PostJsonSignalHubAsync<IEnumerable<T>>(uri, payload);
            return response;
        }

        public async Task<IEnumerable<T>> Distinct<T>(Models.Source source, object payload)
        {
            var uri = $"{source.Url}/distinct";
            var response = await httpClient.PostJsonSignalHubAsync<IEnumerable<T>>(uri, payload);
            return response;
        }
    }

    namespace Payloads
    {
        public class CountPayload
        {
            private CountPayload() { }

            public static object CreateFrom(QueryConfig visualConfig, int size, IEnumerable<FieldMeasure> baseAggregations = null, IEnumerable<FieldDimension> baseDimensions = null, IEnumerable<FieldFilter> additionalFilters = null, string searchText = null)
            {
                var allFilters = new List<FieldFilter>(visualConfig.Filters);
                if (additionalFilters != null)
                    allFilters.AddRange(additionalFilters);

                return new
                {
                    searchText,
                    baseDimensions,
                    baseAggregations,
                    aggregations = visualConfig.MeasureFields,
                    filters = allFilters,
                    variables = visualConfig.Variables,
                    size = size,
                };
            }
        }

        public class TabularPayload
        {
            private TabularPayload() { }

            public static object CreateFrom(IEnumerable<string> fields, IEnumerable<FieldFilter> filters = null, IEnumerable<FieldOrder> orders = null, IEnumerable<Variable> variables = null, string searchText = null, int? size = null)
            {
                return new { searchText, fields, filters, orders, variables, size };
            }

            public static object CreateFrom(IEnumerable<FieldDimension> fields, int size, IEnumerable<FieldFilter> filters = null, IEnumerable<FieldOrder> orders = null, IEnumerable<Variable> variables = null, IEnumerable<FieldFilter> additionalFilters = null, string searchText = null)
            {
                var allFilters = filters == null ? new List<FieldFilter>() : new List<FieldFilter>(filters);
                if (additionalFilters != null)
                    allFilters.AddRange(additionalFilters);

                return new
                {
                    searchText,
                    project = fields.Select(d => $"{d.ResultName}={d.Name}"),
                    filters = allFilters,
                    orders,
                    variables,
                    size,
                };
            }

            public static object CreateFrom(QueryConfig visualConfig, int size, IEnumerable<FieldFilter> additionalFilters = null, string searchText = null)
            {
                var allFilters = new List<FieldFilter>(visualConfig.Filters);
                if (additionalFilters != null)
                    allFilters.AddRange(additionalFilters);

                return new
                {
                    searchText,
                    aggregations = visualConfig.MeasureFields,
                    project = visualConfig.DimensionFields.Select(d => $"{d.ResultName}={d.Name}"),
                    filters = allFilters,
                    orders = visualConfig.OrderFields,
                    variables = visualConfig.Variables,
                    size = size,
                };
            }
        }

        public class AggregationPayload
        {
            private AggregationPayload() { }

            public static object CreateFrom(QueryConfig visualConfig, int size, IEnumerable<Models.FieldFilter> additionalFilters = null, string searchText = null)
            {
                if (visualConfig.DimensionFields.Count() > 0)
                {
                    var allFilters = new List<FieldFilter>(visualConfig.Filters);
                    if (additionalFilters != null)
                        allFilters.AddRange(additionalFilters);

                    return new
                    {
                        searchText,
                        aggregations = visualConfig.MeasureFields,
                        dimensions = visualConfig.DimensionFields,
                        filters = allFilters,
                        orders = visualConfig.OrderFields,
                        variables = visualConfig.Variables,
                        size = size,
                    };
                }

                throw new Exception("Could not create aggregation payload. Check if dimension field is 1 and that measure field are at least one.");
            }
        }

        public class DistinctPayload
        {
            private DistinctPayload() {}

            public static object CreateFrom(IEnumerable<FieldDimension> dimensions, int? size = null, IEnumerable<FieldFilter> filters = null, IEnumerable<FieldOrder> orders = null, IEnumerable<Models.FieldFilter> additionalFilters = null)
            {
                var allFilters = filters == null ? new List<FieldFilter>() : new List<FieldFilter>(filters);
                if (additionalFilters != null)
                    allFilters.AddRange(additionalFilters);

                return new
                {
                    dimensions,
                    filters = allFilters,
                    orders,
                    size,
                };
            }
        }
    }
}
