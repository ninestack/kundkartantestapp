﻿using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;

namespace SignalHub.Shared.Services
{
    public class SourceTranslator
    {
        private readonly SourceService _sourceService;
        private readonly ApplicationState _applicationState;
        //private readonly AuthenticationStateProvider _authenticationStateProvider;
        private readonly ConcurrentDictionary<string, Models.SourceMetadata> _sourcesMetaDatas = new ConcurrentDictionary<string, Models.SourceMetadata>();

        public SourceTranslator(SourceService sourceService, ApplicationState applicationState)//, AuthenticationStateProvider authenticationStateProvider)
        {
            _sourceService = sourceService;
            _applicationState = applicationState;
            //_authenticationStateProvider = authenticationStateProvider;
        }

        public Models.SourceDictionary Translate(Models.Source source, string language = null)
        {
            return TranslateSource(source, GetLanguageOrDefault(language));
        }

        public Models.FieldMetadata Translate(Models.FieldData field, string language = null)
        {
            return TranslateField(field, GetLanguageOrDefault(language));
        }

        public Models.SourceDictionary TranslateSource(Models.Source source, string language = null)
        {
            var entityId = source.Id;

            if (string.IsNullOrWhiteSpace(entityId))
                return new Models.SourceDictionary { Title = source.Title ?? source.Name, Description = source.Description ?? string.Empty, Language = language };

            language = GetLanguageOrDefault(language);

            var sourceMetadata = TryGetSourceMetadata(entityId);
            var meta = sourceMetadata?.SourceDictionaries.FirstOrDefault(s => s.Language == null || s.Language == language);
            if (meta == null)
                return new Models.SourceDictionary { Title = source.Title ?? source.Name, Description = source.Description ?? string.Empty, Language = language };
            else
                return meta;
        }

        public Models.FieldMetadata TranslateField(Models.FieldData field, string language = null)
        {
            var entityId = field.SoureId;

            if (string.IsNullOrWhiteSpace(entityId))
                return new Models.FieldMetadata { /*SoureId = entityId, */Name = field.Name, Title = field.Title ?? field.Name, Description = field.Description ?? string.Empty, Language = language };

            language = GetLanguageOrDefault(language);

            var sourceMetadata = TryGetSourceMetadata(entityId);
            var meta = sourceMetadata?.FieldDictionaries.FirstOrDefault(f => language == null || f.Language == language);
            if (meta == null)
                return new Models.FieldMetadata { /*SoureId = entityId, */Name = field.Name, Title = field.Title ?? field.Name, Description = field.Description ?? string.Empty, Language = language };
            else
                return meta.Fields?.FirstOrDefault(f => f.Name == field.Name) ?? new Models.FieldMetadata {/* SoureId = entityId, */Name = field.Name, Title = field.Title ?? field.Name, Description = field.Description ?? string.Empty, Language = language };
        }

        private Models.SourceMetadata TryGetSourceMetadata(string entityId)
        {
            Models.SourceMetadata sourceMetadata = null;
            if (_sourcesMetaDatas.TryGetValue(entityId, out var items))
                sourceMetadata = items;
            else
            {
                sourceMetadata = Task.Run<Models.SourceMetadata>(() => _sourceService.GetMetadata(entityId)).Result;
                sourceMetadata = sourceMetadata ?? new Models.SourceMetadata { SourceId = entityId, SourceName = string.Empty };
                _sourcesMetaDatas.TryAdd(entityId, sourceMetadata);
            }
            return sourceMetadata;
        }

        private string GetLanguageOrDefault(string language)
        {
            if (language == null)
            {
                //var authState = Task.Run<AuthenticationState>(() => _authenticationStateProvider.GetAuthenticationStateAsync()).Result;
                return Task.Run<Models.User>(() => _applicationState.User()).Result.Language;
            }
            return null;
        }
    }
}
