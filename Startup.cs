using Kundkartan.Shared.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SignalHub.Shared.Extensions;
using System;

namespace KundkartanTestApp
{
    public class Startup
    {
        private SignalHub.Shared.Settings.AuthSettings authSettings;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            var applicationSettingsSection = Configuration.GetSection("ApplicationSettings");
            services.Configure<SignalHub.Shared.Settings.Application>(applicationSettingsSection);
            var isDevEnv = (Environment.GetEnvironmentVariables().Contains("ASPNETCORE_ENVIRONMENT"))
                ? string.Equals(Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"), "Development", StringComparison.OrdinalIgnoreCase)
                : default;

            // SignalHub general
            if (!isDevEnv) services.AddSignalHub("SignalHub", applicationSettingsSection);

            services.AddRazorPages();
            services.AddServerSideBlazor().AddHubOptions(o =>
            {
                o.MaximumReceiveMessageSize = 10 * 1024 * 1024;
            });
            services.AddHttpContextAccessor();

            //var applicationSettingsSection = Configuration.GetSection("ApplicationSettings");
            //authSettings = applicationSettingsSection.Get<SignalHub.Shared.Settings.Application>().Auth;

            services.Configure<Kundkartan.Shared.Settings.Application>(applicationSettingsSection); //kundkartan appSettings
            services.AddSignalHubServices(applicationSettingsSection);
            services.AddKundkartanServices(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
